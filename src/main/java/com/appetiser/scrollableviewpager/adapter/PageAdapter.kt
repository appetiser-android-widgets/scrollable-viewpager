package com.appetiser.scrollableviewpager.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class PageAdapter(fm: FragmentManager, private val fragments: List<Fragment>) : FragmentStatePagerAdapter(fm) {
    override fun getCount(): Int {
        return this.fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return this.fragments[position]
    }

}
