package com.appetiser.scrollableviewpager

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.viewpager.widget.ViewPager

class ScrollableViewPager : ViewPager {

    private var canScroll = true

    constructor(@NonNull context: Context) : super(context) {
        init(null)
    }

    constructor(@NonNull context: Context, @Nullable attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.ScrollableViewPager)
            try {
                array.getBoolean(R.styleable.ScrollableViewPager_canScroll, false).let {
                    canScroll = it
                }
            } finally {
                array.recycle()
            }
        }
        setCanScroll(canScroll)
    }

    fun setCanScroll(canScroll: Boolean) {
        this.canScroll = canScroll
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return canScroll && super.onTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return canScroll && super.onInterceptTouchEvent(ev)
    }
}
